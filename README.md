# DataTools_Tutorial_Demo
Set of jupyter notebooks created by students in CMSE495 (Michigan State University) that demonstrate some useful data science tools. 

This notebook includse an environment.yml file. Create and activate an enviornment using the following commands (Assumes anadonda or miniconda is installed):

conda env create --prefix ./envs --file environment.yml
conda activate ./envs

If you are new to git please review the following self guided curriculum:

- [Git Guide](https://msu-cmse-courses.github.io/cmse802-f20-student/0000--Jupyter-Getting-Started-Guide.html)

Written by:
- Dirk Colbry
- 
